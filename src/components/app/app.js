import { Component } from 'react';
import Lorem from '../lorem/lorem';
import LoremOutput from '../lorem-output/lorem-output';
import LoremControls from '../lorem-controls/lorem-controls';
import './app.css';

class App extends Component{

    constructor(props){
        super(props);
        this.state = {
            text: Lorem.run({
                paragraphs: 3,
                wordsInPar: 50
            }),
        }
    }

    copyText = () => {
        navigator.clipboard.writeText(this.state.text)
    }

    randomText = () => {

        this.setState({
            text: Lorem.run({
                paragraphs: 3,
                wordsInPar: 50
            }),
        })
    }

    updateSettings = (paragraphs, wordsInPar) => {
        this.setState({
            text: Lorem.run({
                paragraphs: paragraphs,
                wordsInPar: wordsInPar
            })
        })
    }

    
    render(){
        return(
            <div className="app bg-dark h-100 p-3">
                <div className="container h-100">
                    <div className="row h-100">
                        <div className="col-md-8 h-100">
                            <LoremOutput text={this.state.text} />
                        </div>
                        <div className="col-md-4 d-flex flex-column justify-content-between">
                            <LoremControls onRandom={this.randomText} onSettingUpdate={this.updateSettings} />
                            <div className="p-1 bg-light rounded-3">
                                <div className="container">
                                <div className="row justify-content-center">
                                <div className="col-auto">
                                    <button type="button" onClick={this.copyText} className="btn btn-primary">העתק</button>
                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default App;