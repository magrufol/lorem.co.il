import { Component } from "react";
import LoremSettings from '../lorem-settings/lorem-settings';
import './lorem-controls.css';

class LoremControls extends Component{

    constructor(props){
        super(props);

    }

    render(){
        return(
            <div className="p-3 bg-light rounded-3">
                <button type="button" onClick={this.props.onRandom} className="btn btn-primary">רענון</button>
                <LoremSettings onSettingUpdate = {this.props.onSettingUpdate} />
            </div>  
        );
    }

}

export default LoremControls;