import { Component } from 'react';
import HTMLReactParser from 'html-react-parser';
import './lorem-output.css';

class LoremOutput extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div id="lorem-output">
                <div className="p-3 bg-light rounded-3 h-100">
                    <div className="container-fluid py-5 h-100">
                        <div className="col-12 h-100" id="output-text">
                            {HTMLReactParser(this.props.text)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoremOutput;
