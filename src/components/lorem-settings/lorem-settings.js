import { Component } from 'react';
import './lorem-settings.css';

class LoremSettings extends Component{

    constructor(props){
        super(props);
        this.state = {
            paragraphs: 3,
            wordsInPar: 50
        }
    }

    onChange = (e) => {
        this.setState({
            [e.target.getAttribute("data-name")]: e.target.value,
        })
    }

    onSubmitSettings = () => {
        this.props.onSettingUpdate(this.state.paragraphs, this.state.wordsInPar)
    }

    render(){
        return(
            <div id="lorem-settings">
                <div className="input-group mt-2 row">
                    <div className="col-md-6">
                        <input type="text" 
                            onChange={this.onChange} 
                            className="form-control mb-1" 
                            placeholder="Paragraphs" 
                            data-name="paragraphs"
                            aria-label="Username" 
                            aria-describedby="addon-wrapping" />
                    </div>
                    
                    <div className="col-md-6">
                        <input type="text" 
                            onChange={this.onChange}
                            data-name="wordsInPar"
                            className="form-control mb-1" 
                            placeholder="Username" 
                            aria-label="Username" 
                            aria-describedby="addon-wrapping" />
                    </div>
                    
                </div>
                <button type="button" onClick={this.onSubmitSettings} className="btn btn-success">קבל</button>
            </div>
        );
    }
}

export default LoremSettings;