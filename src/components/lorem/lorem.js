const Lorem = {
	WORDS_PER_SENTENCE_AVG : 24.460,
	WORDS_PER_SENTENCE_STD : 5.080,
	WORDS : ["לורם", "איפסום", "דולור", "סיט אמט", "קונסקטורר", "אדיפיסינג", "אלית", "נולום", "ארווס", "סאפיאן", "פוסיליס קוויס", "אקווזמן קוואזי", "במר", "מודוף", "אודיפו", "בלאסטיק", "מונופץ", "קליר", "בנפת", "נפקט", "למסון", "בלרק", "וענוף", "נולום", "ארווס", "סאפיאן", "פוסיליס", "קוויס", "אקווזמן", "קוואזי", "במר", "מודוף", "אודיפו", "בלאסטיק", "מונופץ", "קליר", "בנפת", "נפקט", "למסון", "בלרק", "וענוף", "לפרומי", "בלוף", "קינץ", "תתיח", "לרעח", "לת", "צשחמי", "נולום", "ארווס", "סאפיאן", "פוסיליס", "קוויס", "אקווזמן", "הועניב", "היושבב", "שערש", "שמחויט", "שלושע", "ותלברו", "חשלו", "שעותלשך", "וחאית", "נובש", "ערששף", "זותה", "מנק", "הבקיץ", "אפאח", "דלאמת", "יבש", "כאנה", "ניצאחו", "נמרגי", "שהכים", "תוק", "הדש שנרא התידם", "הכייר", "וק", "מוסן", "מנת", "להאמית", "קרהשק", "סכעיט", "דז", "מא", "מנכם", "למטכין", "נשואי", "מנורך", "ליבם", "סולגק", "בראיט", "ולחת", "צורק", "מונחף", "בגורמי", "מגמש", "תרבנך", "וסתעד", "לכנו", "סתשם", "השמה", "לתכי", "מורגם", "בורק", "לתיג", "ישבעס", "קולורס", "מונפרד", "אדנדום", "סילקוף", "מרגשי", "ומרגשח", "עמחליף", "קולהע", "צופעט", "למרקוח", "איבן", "איף", "ברומץ", "כלרשט", "מיחוצים", "קלאצי", "הועניב", "היושבב", "שערש", "שמחויט", "שלושע", "ותלברו", "חשלו", "שעותלשך", "וחאית", "נובש", "ערששף", "זותה", "מנק", "הבקיץ", "אפאח", "דלאמת", "יבש", "כאנה", "ניצאחו", "נמרגי", "שהכים", "תוק", "הדש", "שנרא", "התידם", "הכייר", "וק", "סחטיר", "בלובק.", "תצטנפל", "בלינדו", "למרקל", "אס", "לכימפו", "דול", "צוט", "ומעיוט", "לפתיעם", "ברשג", "ולתיעם גדדיש", "קוויז", "דומור", "ליאמום", "בלינך", "רוגצה", "לפמעט", "מוסן", "מנת", "קונדימנטום", "קורוס", "בליקרה", "נונסטי", "קלובר", "בריקנה", "סטום", "לפריקך", "תצטריק", "לרטי", "צש", "בליא", "מנסוטו", "צמלח", "לביקו", "ננבי", "צמוקו", "בלוקריה", "שיצמה", "ברורק", "לורם", "איפסום", "דולור", "סיט", "אמט,", "קונסקטורר", "אדיפיסינג", "אלית", "סת", "אלמנקום", "ניסי", "נון", "ניבאה", "דס", "איאקוליס", "וולופטה דיאם", "וסטיבולום", "אט", "דולור", "קראס", "אגת", "לקטוס", "וואל", "אאוגו", "וסטיבולום", "סוליסי", "טידום", "בעליק", "קונדימנטום", "קורוס", "בליקרה", "נונסטי", "קלובר", "בריקנה", "סטום", "לפריקך", "תצטריק", "לרטי", "קולורס", "מונפרד", "אדנדום", "סילקוף", "מרגשי", "ומרגשח", "עמחליף", "לורם", "איפסום", "דולור", "סיט", "אמט", "קונסקטורר", "אדיפיסינג", "אלית", "סת", "אלמנקום", "ניסי", "נון", "ניבאה", "דס", "איאקוליס", "וולופטה דיאם", "וסטיבולום", "אט", "דולור", "קראס", "אגת", "לקטוס", "וואל", "אאוגו", "וסטיבולום", "סוליסי", "טידום", "בעליק"],
	run: function({paragraphs, wordsInPar}){
		let res = '';
		for(let i = 0; i < paragraphs; i++){
			res += '<p>' + this.generate(wordsInPar) + '</p>';
		}

		return res;
	},
	generate : function (num_words) {
		var words, ii, position, word, current, sentences, sentence_length, sentence;
		
		/**
		 * @default 100
		 */
		num_words = num_words || 100;
		
		words = [this.WORDS[0], this.WORDS[1]];
		num_words -= 2;
		
		for (ii = 0; ii < num_words; ii++) {
			position = Math.floor(Math.random() * this.WORDS.length);
			word = this.WORDS[position];
			
			if (ii > 0 && words[ii - 1] === word) {
				ii -= 1;
				
			} else {
				words[ii] = word;
			}
		}
		
		sentences = [];
		current = 0;
		
		while (num_words > 0) {
			sentence_length = this.getRandomSentenceLength();
			
			if (num_words - sentence_length < 4) {
				sentence_length = num_words;
			}
			
			num_words -= sentence_length;
			
			sentence = [];
			
			for (ii = current; ii < (current + sentence_length); ii++) {
				sentence.push(words[ii]);
			}
			
			sentence = this.punctuate(sentence);
			current += sentence_length;
			sentences.push(sentence.join(' '));
		}
		
		return sentences.join(' ');
	},
	punctuate : function (sentence) {
		var word_length, num_commas, ii, position;
		
		word_length = sentence.length;
		
		/* End the sentence with a period. */
		sentence[word_length - 1] += '.';
		
		if (word_length < 4) {
			return sentence;
		}
		
		num_commas = this.getRandomCommaCount(word_length);
		
		for (ii = 0; ii <= num_commas; ii++) {
			position = Math.round(ii * word_length / (num_commas + 1));
			
			if (position < (word_length - 1) && position > 0) {
				/* Add the comma. */
				sentence[position] += ',';
			}
		}
		
		/* Capitalize the first word in the sentence. */
		sentence[0] = sentence[0].charAt(0).toUpperCase() + sentence[0].slice(1);
		
		return sentence;
	},
	getRandomCommaCount : function (word_length) {
		var base, average, standard_deviation;
		
		/* Arbitrary. */
		base = 6;
		
		average = Math.log(word_length) / Math.log(base);
		standard_deviation = average / base;
		
		return Math.round(this.gaussMS(average, standard_deviation));
	},
	getRandomSentenceLength : function () {
		return Math.round(
				this.gaussMS(
						this.WORDS_PER_SENTENCE_AVG,
						this.WORDS_PER_SENTENCE_STD
				)
		);
	},
	gauss : function () {
		return (Math.random() * 2 - 1) +
				(Math.random() * 2 - 1) +
				(Math.random() * 2 - 1);
	},
	gaussMS : function (mean, standard_deviation) {
		return Math.round(this.gauss() * standard_deviation + mean);
	}

}


export default Lorem;